const express = require("express");
// const controller = require("./Deprecated/controller.js");
// const readController = require("./controllers/read.controller.js");
// const writeController = require("./controllers/write.controller.js");
const middleware = require("./middleware/auth.middleware.js")
const getAllController = require("./controllers/getAll.controller.js");
const getGameIDController = require("./controllers/getGameID.controller.js");
const getUserDetailsController = require("./controllers/getUserDetails.controller.js");
const checkClaimStatusController = require("./controllers/checkClaimStatus.controller.js");
const dailyScoreController = require("./controllers/dailyScore.controller.js");
const tokensRequestController = require("./controllers/tokensRequest.controller.js");
const route = express.Router();

// Add JWT middleware to verify token before accessing protected routes
const verifyToken = middleware.verifyToken;

route.get("/", getAllController.getAll);

route.get("/:gameID", getGameIDController.getGameID);

route.get("/:gameID/:user", verifyToken, getUserDetailsController.getUserDetails);
  
route.put("/:gameID/login", middleware.login);

route.put("/:gameID/:user/dailyscore", verifyToken, dailyScoreController.dailyScore);
  
/* route.put("/:gameID/:user/totalscore", verifyToken, writeController.totalScore); */

route.put("/:gameID/:user/tokensreq", verifyToken, tokensRequestController.tokensRequest);

route.get("/:gameID/:user/claimstatus", verifyToken, checkClaimStatusController.checkClaimStatus);

module.exports = route;
