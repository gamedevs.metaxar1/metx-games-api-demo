const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

const checkClaimStatus = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { autosigner_url } = req.query;

    const ref = admin.database().ref(`${gameID}/${user}`);
    const tokensReqRef = ref.child('c_TokensReq');
    const tokensClaimRef = ref.child('d_TokensClaim');
    const scoreRef = ref.child('b_Score');

    // Retrieve the tokensRequested amount, tokensClaimed and totalScore from the database
    const tokensReqSnapshot = await tokensReqRef.once('value');
    const tokensClaimSnapshot = await tokensClaimRef.once('value');
    const scoreSnapshot = await scoreRef.once('value');

    const tokensReqVal = tokensReqSnapshot.val();

    if (!tokensReqVal) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for token request.",
        data: {},
      });
    }

    let tokensRequestedAmount = tokensReqVal.a_TokensReq;
    let tokensClaimedVal = tokensClaimSnapshot.val();
    let tokensClaimed = tokensClaimedVal ? tokensClaimedVal.g_TokensClaimed : 0;
    let totalScore = scoreSnapshot.val().d_TotalScore;

    // Ensure tokensRequestedAmount, tokensClaimed and totalScore are numbers, if not default them to 0
    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof tokensClaimed !== 'number') tokensClaimed = 0;
    if (typeof totalScore !== 'number') totalScore = 0;

    const hash = tokensReqVal.b_TxnHash;

    // If there is no token request made or no transaction hash, return a message to user and do nothing
    if (!tokensRequestedAmount || !hash) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no transaction hash available.",
        data: {},
      });
    }

    // Call the autosigner API to get the transaction status
    const response = await axios.get(`${autosigner_url}/api/v1/transactions/${hash}`, {
      headers: { Authorization: `Bearer ${process.env.BEARER_TOKEN}` },
    });

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 

    // Check transaction status and perform updates accordingly
    const transaction = response.data.viewModel;
    let tokensClaimedThisTime = 0;  // Track tokens claimed in this request

    // Check if transaction is successful
    if (transaction.group === 'confirmed' && transaction.status === 'Success') {
      tokensClaimedThisTime = tokensRequestedAmount;  // Tokens claimed is equal to the tokens requested
      tokensClaimed += tokensClaimedThisTime;  // Update total tokens claimed

      // reset daily score, reset tokens requested, update tokens claimed
      await Promise.all([
        tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
        scoreRef.update({ a_DailyScore: 0, b_DS_Updated: time}),
        tokensClaimRef.update({ g_TokensClaimed: tokensClaimed, h_TC_Updated: time}),
      ]);

      const viewModel = {
        success: true,
        message: "Claim status checked and handled",
        data: {
          txnStatus: transaction.status,
          time,
          totalScore,
          tokensRequestedAmount,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      };
      postHistory(gameID, user, tokensClaimedThisTime, hash);
      return res.status(200).json(viewModel);
 
      
    } else if (transaction.status === 'Fail') { 
      // Transaction failed but within 5 minutes, so we don't update tokensClaimed or totalScore, and don't reset tokensReq
      // Do nothing
      return res.status(400).json({
        success: false,
        message: 'Transation not completed'
      })
    } else if (transaction.status !== 'Success' && now.diff(moment(transaction.deadline), 'minutes') > 5) { 
      // after 5 mins and txn status still not success: retrieve data from tokensreq to total score, tokensreq reset, daily no change
      totalScore += tokensRequestedAmount;

      await Promise.all([
        scoreRef.update({ d_TotalScore: totalScore, e_TS_Updated: time }),
        tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
      ]);

      return res.status(200).json({
        success: false,
        message: "Transaction failed after 5 minutes. The requested tokens have been returned to your score.",
        data: {
          txnStatus: transaction.status,
          time,
          totalScore,
          tokensRequestedAmount,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      });
    }  
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

function postHistory(gameID, user, amount, hash) {
  const now = moment().tz(timezone);
  const time = now.format('YYYY-MM-DD HH:mm:ss'); 
  const ref = admin.database().ref(`${gameID}/${user}/d_TokensClaim/z_History`);
  const historyData = {
    [time]: {
      tokens: amount,
      txnhash: hash,
    },
  };
  ref.update(historyData);
}

module.exports = {
  checkClaimStatus
};
