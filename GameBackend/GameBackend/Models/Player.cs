using System;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GameBackend.Models
{
    public class Player
    {
        [BsonId]
        [BsonElement("userId")]
        [JsonPropertyName("userId")]
        public string? UserId { get; set; } = null;

        [BsonElement("user")]
        [JsonPropertyName("user")]
        public User UserData { get; set; } = new User();

        [BsonElement("score")]
        [JsonPropertyName("score")]
        public Score ScoreData { get; set; } = new Score();

        [BsonElement("tokensReq")]
        [JsonPropertyName("tokensReq")]
        public TokensReq TokensReqData { get; set; } = new TokensReq();

        [BsonElement("tokensClaim")]
        [JsonPropertyName("tokensClaim")]
        public TokensClaim TokensClaimData { get; set; } = new TokensClaim();
    }

    public class User
    {
        [BsonElement("gameId")]
        [JsonPropertyName("gameId")]
        public string? GameID { get; set; }

        [BsonElement("tokenId")]
        [JsonPropertyName("tokenId")]
        public string? TokenID { get; set; } = null!;

        [BsonElement("loginTime")]
        [JsonPropertyName("loginTime")]
        public DateTime LoginTime { get; set; }
    }

    public class Score
    {
        [BsonElement("dailyScore")]
        [JsonPropertyName("dailyScore")]
        public int DailyScore { get; set; }

        [BsonElement("ds_updated")]
        [JsonPropertyName("ds_updated")]
        public DateTime DSUpdated { get; set; }

        [BsonElement("totalScore")]
        [JsonPropertyName("totalScore")]
        public int TotalScore { get; set; }

        [BsonElement("ts_updated")]
        [JsonPropertyName("ts_updated")]
        public DateTime TSUpdated { get; set; }
    }

    public class TokensReq
    {
        [BsonElement("amount")]
        [JsonPropertyName("amount")]
        public int TokensReqValue { get; set; }

        [BsonElement("txn_hash")]
        [JsonPropertyName("txn_hash")]
        public string? Txnhash { get; set; }

        [BsonElement("tr_updated")]
        [JsonPropertyName("tr_updated")]
        public DateTime TRUpdated { get; set; }
    }

    public class TokensClaim
    {
        [BsonElement("amount")]
        [JsonPropertyName("amount")]
        public int TokensClaimed { get; set; }

        [BsonElement("tc_updated")]
        [JsonPropertyName("tc_updated")]
        public DateTime TCUpdated { get; set; }

        /* [BsonElement("history")]
        [JsonPropertyName("history")]
        public Dictionary<DateTime, HistoryItem> History { get; set; } = new Dictionary<DateTime, HistoryItem>(); */
    }

    /* public class HistoryItem
    {
        [BsonElement("tokens")]
        [JsonPropertyName("tokens")]
        public int Tokens { get; set; }

        [BsonElement("txn_hash")]
        [JsonPropertyName("txn_hash")]
        public string? Txnhash { get; set; } = null!;
    } */
}
