using GameBackend.Models;
using GameBackend.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GameBackend.Controllers
{
    [ApiController]
    [Route("{collectionName}/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<Player>> GetUser(string collectionName, string userId)
        {
            var user = await _userService.GetUser(collectionName, userId);

            if (user is null)
            {
                return NotFound();
            }

            return user;
        }

        [HttpPost("{userId}")]
        public async Task<IActionResult> CreateUser(string collectionName, string userId, [FromBody] Player newUser)
        {
            if (string.IsNullOrEmpty(newUser?.UserData?.GameID) || string.IsNullOrEmpty(newUser?.UserData?.TokenID) || string.IsNullOrEmpty(newUser?.UserId))
            {
                return BadRequest(new
                {
                    success = false,
                    message = "Missing data in User object"
                });
            }

            newUser.UserId = userId;

            try
            {
                await _userService.CreateNewUser(collectionName, userId, newUser);

                var viewModel = new
                {
                    success = true,
                    message = "User created successfully",
                    data = newUser,
                };
                return StatusCode(201, viewModel);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                return StatusCode(500, new
                {
                    success = false,
                    message = "Internal server error"
                });
            }
        }

        [HttpPut("{userId}")]
        public async Task<IActionResult> UpdateUser(string collectionName, string userId, [FromBody] User updatedUser)
        {
            var user = await _userService.GetUser(collectionName, userId);

            if (user is null)
            {
                return NotFound();
            }

            await _userService.LoginUser(collectionName, userId, updatedUser);

            return NoContent();
        }
    }
}
