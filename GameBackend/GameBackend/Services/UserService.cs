using GameBackend.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Threading.Tasks;
using System;

namespace GameBackend.Services
{
    public class UserService : IUserService
    {
        private readonly IMongoDatabase _database;

        public UserService(IOptions<GameBackendDatabaseSettings> gameBackendDatabaseSettings)
        {
            var mongoClient = new MongoClient(gameBackendDatabaseSettings.Value.ConnectionString);
            _database = mongoClient.GetDatabase(gameBackendDatabaseSettings.Value.DatabaseName);
        }

        // Method to get the collection by name
        private IMongoCollection<Player> GetCollectionByName(string collectionName)
        {
            return _database.GetCollection<Player>(collectionName);
        }

        public async Task<Player?> GetUser(string collectionName, string userId)
        {
            var collection = GetCollectionByName(collectionName);
            return await collection.Find(x => x.UserId == userId).FirstOrDefaultAsync();
        }

        public async Task CreateNewUser(string collectionName, string userId, Player newUser)
        {
            newUser.UserId = userId;
            newUser.UserData.GameID = collectionName;
            var collection = GetCollectionByName(collectionName);
            await collection.InsertOneAsync(newUser);
        }

        public async Task LoginUser(string collectionName, string userId, User updatedUser)
        {
            var collection = GetCollectionByName(collectionName);

            var filter = Builders<Player>.Filter.Eq(x => x.UserId, userId);

            var update = Builders<Player>.Update
                .Set(x => x.UserData.LoginTime, DateTime.UtcNow);

            await collection.UpdateOneAsync(filter, update);
        }
    }
}
