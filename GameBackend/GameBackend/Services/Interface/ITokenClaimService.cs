using GameBackend.Models;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface ITokenClaimService
    {
        Task<TokensClaim?> CheckClaimStatus(string collectionName, string id);
    }
}
