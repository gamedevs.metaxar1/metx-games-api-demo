using GameBackend.Models;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface ITokenRequestService
    {
        Task UpdateTokenReq(string collectionName, string id, Score updateTotalScore, TokensReq updateTokenReq, TokensClaim updateTokenClaim);
    }
}
