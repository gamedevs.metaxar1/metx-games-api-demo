using GameBackend.Models;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface IUserService
    {
        Task<Player?> GetUser(string collectionName, string id);
        Task CreateNewUser(string collectionName, string userId, Player newUser);
        Task LoginUser(string collectionName, string id, User updatedUser);
    }
}
