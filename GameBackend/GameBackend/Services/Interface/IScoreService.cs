using GameBackend.Models;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface IScoreService
    {
        Task UpdateScore(string collectionName, string userId, int score, int? limit = null);
    }
}
