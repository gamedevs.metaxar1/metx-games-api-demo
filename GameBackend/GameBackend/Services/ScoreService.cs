using GameBackend.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Threading.Tasks;
using System;

namespace GameBackend.Services
{
    public class ScoreService : IScoreService
    {
        private readonly IMongoDatabase _database;

        public ScoreService(IOptions<GameBackendDatabaseSettings> gameBackendDatabaseSettings)
        {
            var mongoClient = new MongoClient(gameBackendDatabaseSettings.Value.ConnectionString);
            _database = mongoClient.GetDatabase(gameBackendDatabaseSettings.Value.DatabaseName);
        }

        // Method to get the collection by name
        private IMongoCollection<Player> GetCollectionByName(string collectionName)
        {
            return _database.GetCollection<Player>(collectionName);
        }

        public async Task UpdateScore(string collectionName, string userId, int score, int? limit = null)
        {
            int newScore = score;
            var collection = GetCollectionByName(collectionName);

            // Retrieve the player document by the provided userId
            var player = await collection.Find(x => x.UserId == userId).FirstOrDefaultAsync();

            if (player == null)
            {
                throw new ArgumentException("Player not found.");
            }

            // Retrieve the current daily score and total score from the player object
            var dailyScore = player.ScoreData.DailyScore;
            var totalScore = player.ScoreData.TotalScore;

            // Calculate the new daily score
            var now = DateTime.UtcNow;
            if (limit.HasValue && dailyScore >= limit.Value)
            {
                throw new InvalidOperationException("You have reached your score limit for today");
            }
            if (limit.HasValue && dailyScore + newScore > limit.Value)
            {
                newScore = limit.Value - dailyScore; // Adjust newScore to limit the dailyScore
            }

            // Update the player's score data
            dailyScore += newScore;
            totalScore += newScore;

            var update = Builders<Player>.Update
                .Set(x => x.ScoreData.DailyScore, dailyScore)
                .Set(x => x.ScoreData.DSUpdated, now)
                .Set(x => x.ScoreData.TotalScore, totalScore)
                .Set(x => x.ScoreData.TSUpdated, now);

            await collection.UpdateOneAsync(x => x.UserId == userId, update);
        }
    }

}